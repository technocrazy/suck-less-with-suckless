# Suck Less with Suckless!

This repository is for Felix's builds of [Suckless](https://suckless.org) programs. These are the programs that are currently in the repository:

* dmenu
* st

I may fuck with dwm or surf in the future, but this is all for now.

# Installation

Most of these programs will only need the following commands to build:

```sh
make
sudo make install
make clean
```

# dmenu

dmenu is great for application launching and running scripts. My build uses the following patches:

* [border](https://tools.suckless.org/dmenu/patches/border/)
* [center](https://tools.suckless.org/dmenu/patches/center/)

Preview image:

![dmenu preview image](dmenu/dmenu_example.png)

# st

st is the best terminal emulator, and anyone who thinks otherwise is wrong, because the only time I'm wrong is when I doubt myself, since I'm always right in the end. My build uses the following patches:

* [alpha](https://st.suckless.org/patches/alpha/)
* [clipboard](https://st.suckless.org/patches/clipboard/)
* [copyurl](https://st.suckless.org/patches/copyurl/)
* [open_copied_url](https://st.suckless.org/patches/open_copied_url/)
* [scrollback](https://st.suckless.org/patches/scrollback/)

Preview image (showed with Neovim):

![st preview image](st/st_example.png)
